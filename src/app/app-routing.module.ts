import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { D1Component } from './componentes/d1/d1.component';
import { D2Component } from './componentes/d2/d2.component';
import { E1Component } from './componentes/e1/e1.component';
import { GuiaComponent } from './componentes/guia/guia.component';
import { HomeComponent } from './componentes/home/home.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent},
  { path: 'guia', component: GuiaComponent},
  { path: 'd1', component: D1Component},
  { path: 'd2', component: D2Component},
  { path: 'e1', component: E1Component},



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
