import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './componentes/menu/menu.component';
import { HomeComponent } from './componentes/home/home.component';
import { D1Component } from './componentes/d1/d1.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { D2Component } from './componentes/d2/d2.component';
import { E1Component } from './componentes/e1/e1.component';
import { GuiaComponent } from './componentes/guia/guia.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HomeComponent,
    D1Component,
    D2Component,
    E1Component,
    GuiaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
