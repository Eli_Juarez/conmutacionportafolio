import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-e1',
  templateUrl: './e1.component.html',
  styleUrls: ['./e1.component.css']
})
export class E1Component implements OnInit {

  constructor(public modal:NgbModal) { }

  ngOnInit(): void {
  }

}
