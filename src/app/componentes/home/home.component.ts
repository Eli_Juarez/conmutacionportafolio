import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  d1(){
    this.router.navigate(['/d1']);
  }

  d2(){
    this.router.navigate(['/d2']);
  }

  e1(){
    this.router.navigate(['/e1']);
  }
}
